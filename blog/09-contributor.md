---
date: 9999-01-01T00:01 #列表按日期排序，不要更改日期
slug: contributor
title: 4 贡献者与开源项目
authors: []
tags: []
---

### 编写与文稿修正作者
:::tip
此部分暂时需要转到[gitee.com/jimmykmi/miot-wiki/contributors](https://gitee.com/jimmykmi/miot-wiki/contributors)查看。
:::

[//]: # (<iframe src="/contributor"></iframe>)

### 服务提供商
- SERVERLESS 提供商[AMAZON.COM](https://vercel.com/)
- CDN 提供商[VERCEL.COM](https://vercel.com/)
- WAF 提供商[CLOUDFLARE.COM](https://www.cloudflare.com/)

### 鸣谢开源项目
- Docusaurus[DOCUSAURUS.IO](https://docusaurus.io/)

<!-- :::tip -->

<!-- Use the power of React to create interactive blog posts. -->

<!-- ```js -->
<!-- <button onClick={() => alert('button clicked!')}>Click me!</button> -->
<!-- ``` -->

<!-- <button onClick={() => alert('button clicked!')}>Click me!</button> -->

<!-- ::: -->
