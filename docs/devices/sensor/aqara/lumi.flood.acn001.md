---
sidebar_label: 'Aqara 水浸传感器 E1'
---
# Aqara 水浸传感器 E1

2021-07-15 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=lumi.flood.acn001/) | [说明书](https://home.mi.com/views/introduction.html?model=lumi.flood.acn001&region=cn)

![lumi.flood.acn001](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-product-center/602b2925ac5b707c2f62970ac9e63a93_developer_1552375117c8gh9c45.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=K3bUVtmmsV+OKDEYi5ULr+Ncyqs=)

## 规格  
> 
**设备 ID** ：lumi.flood.acn001  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
浸水报警、浸水解除
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
