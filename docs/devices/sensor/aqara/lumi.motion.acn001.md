---
sidebar_label: 'Aqara 人体传感器 E1'
---
# Aqara 人体传感器 E1

2022-05-10 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=lumi.motion.acn001/) | [说明书](https://home.mi.com/views/introduction.html?model=lumi.motion.acn001&region=cn)

![lumi.motion.acn001](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-product-center/69a264fe5a5f0e60907c47a11a255a3a_developer_1568790614naen5iow.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=HvHqmvqxcB3DEAfFcOuh0M876sw=)

## 规格  
> 
**设备 ID** ：lumi.motion.acn001  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
有人移动、2分钟无人移动、5分钟无人移动、10分钟无人移动、20分钟无人移动、30分钟无人移动、有人移动且亮度高于/低于指定值
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
