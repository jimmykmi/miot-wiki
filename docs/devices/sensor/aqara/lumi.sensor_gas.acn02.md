---
sidebar_label: 'Aqara 天然气报警器'
---
# Aqara 天然气报警器

2021-09-27 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=lumi.sensor_gas.acn02/) | [说明书](https://home.mi.com/views/introduction.html?model=lumi.sensor_gas.acn02&region=cn)

![lumi.sensor_gas.acn02](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-user-center/developer_1678871035585WB1ucTsX.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=whJXW0zDHcp1rjC9WLEsdyxyHHY=)

## 规格  
> 
**设备 ID** ：lumi.sensor_gas.acn02  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
天然气泄漏报警、天然气泄漏报警解除
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
