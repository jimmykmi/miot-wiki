---
sidebar_label: 'Aqara人体传感器'
---
# Aqara人体传感器

2017-11-16 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=lumi.sensor_motion.aq2/) | [说明书](https://home.mi.com/views/introduction.html?model=lumi.sensor_motion.aq2&region=cn)

![lumi.sensor_motion.aq2](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-user-center/developer_1679067441595vlqPZJ1q.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=wGSYP0kMlBa5GYWq8P0vVllOqMo=)

## 规格  
> 
**设备 ID** ：lumi.sensor_motion.aq2  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
有人移动、2分钟无人移动、5分钟无人移动、10分钟无人移动、20分钟无人移动、30分钟无人移动、有人移动且亮度暗
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
