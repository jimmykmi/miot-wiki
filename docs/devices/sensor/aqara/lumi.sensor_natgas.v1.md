---
sidebar_label: '天然气报警器'
---
# 天然气报警器

2017-11-16 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=lumi.sensor_natgas.v1/) | [说明书](https://home.mi.com/views/introduction.html?model=lumi.sensor_natgas.v1&region=cn)

![lumi.sensor_natgas.v1](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-user-center/developer_16790475117888q8xtb1r.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=u6h1ADHCf9noOTpO17ylKNDQffI=)

## 规格  
> 
**设备 ID** ：lumi.sensor_natgas.v1  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
气体泄漏报警
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
