---
sidebar_label: '烟雾报警器'
---
# 烟雾报警器

2017-11-16 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=lumi.sensor_smoke.v1/) | [说明书](https://home.mi.com/views/introduction.html?model=lumi.sensor_smoke.v1&region=cn)

![lumi.sensor_smoke.v1](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-user-center/developer_1679047511956aOnu2Tkd.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=RMoStYPzdkFfNLi9aH/QAf57iHk=)

## 规格  
> 
**设备 ID** ：lumi.sensor_smoke.v1  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
火警报警
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
