---
sidebar_label: 'Aqara水浸传感器'
---
# Aqara水浸传感器

2017-11-16 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=lumi.sensor_wleak.aq1/) | [说明书](https://home.mi.com/views/introduction.html?model=lumi.sensor_wleak.aq1&region=cn)

![lumi.sensor_wleak.aq1](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-user-center/developer_167904751322046Y5gwL4.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=2c6rM7dtChx8ky/J4wahw8tFD6c=)

## 规格  
> 
**设备 ID** ：lumi.sensor_wleak.aq1  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
浸水报警、浸水解除
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
