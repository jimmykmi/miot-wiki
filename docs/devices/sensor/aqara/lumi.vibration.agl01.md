---
sidebar_label: 'Aqara动静贴T1'
---
# Aqara动静贴T1

2021-03-08 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=lumi.vibration.agl01/) | [说明书](https://home.mi.com/views/introduction.html?model=lumi.vibration.agl01&region=cn)

![lumi.vibration.agl01](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-product-center/developer_1585557843300pq5uL7Oz.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=rsdT4gjrk72iWuh+BbYR/FbIPws=)

## 规格  
> 
**设备 ID** ：lumi.vibration.agl01  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
感应到三击、感应到移动
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
