---
sidebar_label: 'Aqara动静贴'
---
# Aqara动静贴

2018-08-14 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=lumi.vibration.aq1/) | [说明书](https://home.mi.com/views/introduction.html?model=lumi.vibration.aq1&region=cn)

![lumi.vibration.aq1](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-user-center/developer_1679047513058PKBwoHIA.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=hQOxj5rjgI2fJ2eN+KsH/a4cny4=)

## 规格  
> 
**设备 ID** ：lumi.vibration.aq1  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
感应到震动、感应到跌落、感应到倾斜
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
