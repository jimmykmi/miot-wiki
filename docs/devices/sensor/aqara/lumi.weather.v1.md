---
sidebar_label: 'Aqara温湿度传感器'
---
# Aqara温湿度传感器

2017-11-16 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=lumi.weather.v1/) | [说明书](https://home.mi.com/views/introduction.html?model=lumi.weather.v1&region=cn)

![lumi.weather.v1](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-user-center/developer_1679047512600zbW7yhH9.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=3ezhZV8rIzl0ZtWAbskTNuvfVEA=)

## 规格  
> 
**设备 ID** ：lumi.weather.v1  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
高于指定温度、低于指定温度、高于指定湿度、低于指定湿度
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
