---
sidebar_label: '未来居人体移动传感器'
---
# 未来居人体移动传感器

1970-01-01 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=bymiot.motion.v1/) | [说明书](https://home.mi.com/views/introduction.html?model=bymiot.motion.v1&region=cn)

![bymiot.motion.v1](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-product-center/developer_163091121119737wB6s7C.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=oSv4cv/yBAeE8y/AxCBfHmssEko=)

## 规格  
> 
**设备 ID** ：bymiot.motion.v1  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event

:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
