---
sidebar_label: '未来居强电插卡取电'
---
# 未来居强电插卡取电

2021-02-01 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=bymiot.sensor_occupy.v3/) | [说明书](https://home.mi.com/views/introduction.html?model=bymiot.sensor_occupy.v3&region=cn)

<!-- ![bymiot.sensor_occupy.v3]() -->

## 规格  
> 
**设备 ID** ：bymiot.sensor_occupy.v3  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
插卡、拔卡、继电器断开
:::

:::condition

:::

:::action
通电、断电
:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
