---
sidebar_label: '家用可燃气体探测器'
---
# 家用可燃气体探测器

2022-01-27 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=yuemee.sensor_gas.0605/) | [说明书](https://home.mi.com/views/introduction.html?model=yuemee.sensor_gas.0605&region=cn)

![yuemee.sensor_gas.0605](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-user-center/developer_1679047809757RfTmAQ2A.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=wD3RSDkXlutYaykwXQjEWrZ3rF4=)

## 规格  
> 
**设备 ID** ：yuemee.sensor_gas.0605  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
气体泄漏报警解除、天然气泄漏报警、一氧化碳泄露报警
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
