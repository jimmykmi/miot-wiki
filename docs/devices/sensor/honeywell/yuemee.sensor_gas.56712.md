---
sidebar_label: '家用可燃气体探测器'
---
# 家用可燃气体探测器

2021-10-20 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=yuemee.sensor_gas.56712/) | [说明书](https://home.mi.com/views/introduction.html?model=yuemee.sensor_gas.56712&region=cn)

![yuemee.sensor_gas.56712](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-product-center/d14970845e6541ec0e20817800c0ffb8_1629078544232.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=32JCgktkT6Ym5SpABXfGKeBRRFM=)

## 规格  
> 
**设备 ID** ：yuemee.sensor_gas.56712  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
气体泄漏报警解除、天然气泄漏报警、一氧化碳泄漏报警
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
