---
sidebar_label: '子擎存在传感器 Lite'
---
# 子擎存在传感器 Lite

2022-09-21 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=izq.sensor_occupy.24/) | [说明书](https://home.mi.com/views/introduction.html?model=izq.sensor_occupy.24&region=cn)

![izq.sensor_occupy.24](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-product-center/9f208cd1c2123dd2afdf61827437798f_1659538743846.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=0LzGda5Pf7ViME5hmvtpnW3dgvI=)

## 规格  
> 
**设备 ID** ：izq.sensor_occupy.24  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
无人存在、无人存在指定时间后、有人进入、有人进入指定时间后、有人存在、有人移动、有人微动、有人静态、有人接近、有人远离、目标距离小于（厘米）、目标距离大于（厘米）、光照度小于、光照度大于
:::

:::condition

:::

:::action
设置最大侦测距离、设置无人判定时间、只侦测生物动作、侦测所有的动作、开指示灯、关指示灯、开关指示灯
:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
