---
sidebar_label: 'LineHope门窗传感器TM1'
---
# LineHope门窗传感器TM1

2022-11-09 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=line.magnet.tm1/) | [说明书](https://home.mi.com/views/introduction.html?model=line.magnet.tm1&region=cn)

![line.magnet.tm1](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-product-center/56a091abad620946827b732f8cf3ec77_1661413701028.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=gVaOlNMvcUYoGK+qmRLkj3ZF+2k=)

## 规格  
> 
**设备 ID** ：line.magnet.tm1  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
打开、关闭、设备被强行拆除
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
