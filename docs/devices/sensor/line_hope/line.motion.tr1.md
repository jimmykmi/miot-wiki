---
sidebar_label: 'LineHope人体移动传感器TR1'
---
# LineHope人体移动传感器TR1

2022-11-15 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=line.motion.tr1/) | [说明书](https://home.mi.com/views/introduction.html?model=line.motion.tr1&region=cn)

![line.motion.tr1](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-product-center/f89795eee1b3d8f9328ddbef5afb65c4_1661420453105.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=LijkhEdnJcLpBma3BBZJgKtcoVg=)

## 规格  
> 
**设备 ID** ：line.motion.tr1  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
有人经过
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
