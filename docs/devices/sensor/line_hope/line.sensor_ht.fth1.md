---
sidebar_label: '方舟鱼温湿度传感器TH1'
---
# 方舟鱼温湿度传感器TH1

2023-05-16 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=line.sensor_ht.fth1/) | [说明书](https://home.mi.com/views/introduction.html?model=line.sensor_ht.fth1&region=cn)

![line.sensor_ht.fth1](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-product-center/7e9e50468af702cd65d7cb38a5cc35d1_1661423356271.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=ZpkmOjxbSoT1DRsZJCObyorqzLE=)

## 规格  
> 
**设备 ID** ：line.sensor_ht.fth1  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
温度高于、温度低于、湿度高于、湿度低于
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
