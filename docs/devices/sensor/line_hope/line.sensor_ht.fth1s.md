---
sidebar_label: '方舟鱼温湿度传感器TH1S'
---
# 方舟鱼温湿度传感器TH1S

2023-05-16 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=line.sensor_ht.fth1s/) | [说明书](https://home.mi.com/views/introduction.html?model=line.sensor_ht.fth1s&region=cn)

![line.sensor_ht.fth1s](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-product-center/dbd66e54f2e329f356824318d2d05c03_1661421486508.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=+5G1ZKRK5rMCy3qOqehsP8JnoYQ=)

## 规格  
> 
**设备 ID** ：line.sensor_ht.fth1s  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
湿度高于、湿度低于、温度高于、温度低于
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
