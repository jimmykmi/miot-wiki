---
sidebar_label: '领普人体传感器'
---
# 领普人体传感器

2022-03-09 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=linp.motion.h1/) | [说明书](https://home.mi.com/views/introduction.html?model=linp.motion.h1&region=cn)

![linp.motion.h1](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-user-center/developer_16790689940574VPpZCdd.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=YFZZpDvU+l0lK+ww4HqTyFAoAY0=)

## 规格  
> 
**设备 ID** ：linp.motion.h1  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
有人移动、2分钟内无人移动、5分钟内无人移动、10分钟内无人移动、20分钟内无人移动、30分钟内无人移动、有人移动且光照低于
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
