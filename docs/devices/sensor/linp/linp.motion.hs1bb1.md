---
sidebar_label: '领普人体传感器2'
---
# 领普人体传感器2

2022-08-26 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=linp.motion.hs1bb1/) | [说明书](https://home.mi.com/views/introduction.html?model=linp.motion.hs1bb1&region=cn)

![linp.motion.hs1bb1](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-product-center/724f9aec2d743dc63db154e3a3a0cc19_1642737401162.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=fNtt9X838dphtRHD459cR3EWsUc=)

## 规格  
> 
**设备 ID** ：linp.motion.hs1bb1  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
无人移动超过2分钟、无人移动超过5分钟、无人移动超过10分钟、无人移动超过30分钟、无人移动超过1分钟、有人移动且光照高于、有人移动且光照低于、有人移动
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
