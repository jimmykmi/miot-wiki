---
sidebar_label: 'MIANCHU免触传感器'
---
# MIANCHU免触传感器

2023-02-27 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=mipin.motion.mc1/) | [说明书](https://home.mi.com/views/introduction.html?model=mipin.motion.mc1&region=cn)

![mipin.motion.mc1](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-product-center/688ae9e434052154cb6dfdc96f94cb33_1671716814455.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=bcfxyM9OHIT7+SCZrXP334r7ONY=)

## 规格  
> 
**设备 ID** ：mipin.motion.mc1  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
免触、免触拉、免触推、倒计时结束、倒计时取消、倒计时开始、悬停、双挥、单挥、离开（持续时长）、进入（持续时长）、双移开始（持续时长）、双移结束（持续时长）、进入和离开、进入、离开、双移开始、双移结束
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
