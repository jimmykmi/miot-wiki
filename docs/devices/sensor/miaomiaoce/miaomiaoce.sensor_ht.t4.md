---
sidebar_label: '秒秒测蓝牙温湿度计2'
---
# 秒秒测蓝牙温湿度计2

2023-01-17 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=miaomiaoce.sensor_ht.t4/) | [说明书](https://home.mi.com/views/introduction.html?model=miaomiaoce.sensor_ht.t4&region=cn)

![miaomiaoce.sensor_ht.t4](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-product-center/28810ceddb5953e8554337cb787942ae_1668162204787.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=ZDtiJjZajFK6JPsvOX3+5dx58YU=)

## 规格  
> 
**设备 ID** ：miaomiaoce.sensor_ht.t4  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
低于指定湿度、高于指定湿度、低于指定温度、高于指定温度
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
