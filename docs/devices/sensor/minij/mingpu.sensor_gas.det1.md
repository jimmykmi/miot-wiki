---
sidebar_label: '云檬宝 燃气报警器'
---
# 云檬宝 燃气报警器

2020-11-20 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=mingpu.sensor_gas.det1/) | [说明书](https://home.mi.com/views/introduction.html?model=mingpu.sensor_gas.det1&region=cn)

![mingpu.sensor_gas.det1](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-product-center/developer_1596420974085Pl5TIooe.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=ONmMaRa7HfXlgfvjH0EvOoRzUBE=)

## 规格  
> 
**设备 ID** ：mingpu.sensor_gas.det1  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
探测到燃气泄漏
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
