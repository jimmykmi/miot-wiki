---
sidebar_label: '罗威亚门窗传感器'
---
# 罗威亚门窗传感器

2022-07-04 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=mirsz.magnet.mc500/) | [说明书](https://home.mi.com/views/introduction.html?model=mirsz.magnet.mc500&region=cn)

![mirsz.magnet.mc500](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-user-center/developer_16790680297635v1ud6Jc.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=PpM7IFKdUl8Eqw+Far6rkza0f1Q=)

## 规格  
> 
**设备 ID** ：mirsz.magnet.mc500  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
门窗打开、门窗关闭、触发防拆报警
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
