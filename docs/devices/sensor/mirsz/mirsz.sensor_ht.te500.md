---
sidebar_label: '罗威亚温湿度传感器'
---
# 罗威亚温湿度传感器

2022-06-27 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=mirsz.sensor_ht.te500/) | [说明书](https://home.mi.com/views/introduction.html?model=mirsz.sensor_ht.te500&region=cn)

![mirsz.sensor_ht.te500](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-user-center/developer_1679068029522Uv72QPcl.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=DBpcpKmeTVPkJ+SpkphaxlJ2Nb8=)

## 规格  
> 
**设备 ID** ：mirsz.sensor_ht.te500  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
温度高于、温度低于、湿度高于、湿度低于
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
