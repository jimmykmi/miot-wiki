---
sidebar_label: '人体感应器'
---
# 人体感应器

1970-01-01 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=cchome.motion.v1/) | [说明书](https://home.mi.com/views/introduction.html?model=cchome.motion.v1&region=cn)

![cchome.motion.v1](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-product-center/developer_1574125610562qVbBol5O.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=7Mlrowp9EMI0AzW2DjIQS4SyngU=)

## 规格  
> 
**设备 ID** ：cchome.motion.v1  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event

:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
