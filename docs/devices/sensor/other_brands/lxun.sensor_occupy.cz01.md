---
sidebar_label: 'LX存在传感器'
---
# LX存在传感器

2023-07-06 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=lxun.sensor_occupy.cz01/) | [说明书](https://home.mi.com/views/introduction.html?model=lxun.sensor_occupy.cz01&region=cn)

![lxun.sensor_occupy.cz01](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-product-center/00313c5224a844b973661f1488acddd1_1686810329360.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=tzcD6hq6VdtWVoN+d9mw936Jna8=)

## 规格  
> 
**设备 ID** ：lxun.sensor_occupy.cz01  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
有人存在、无人存在、无人存在指定时间后、有人存在指定时间后、光照度低于、光照度高于
:::

:::condition

:::

:::action
状态指示灯开启、状态指示灯关闭、设置无人判断时间、感应功能开启、感应功能关闭
:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
