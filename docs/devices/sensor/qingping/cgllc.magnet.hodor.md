---
sidebar_label: '青萍门窗开合传感器'
---
# 青萍门窗开合传感器

2021-01-12 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=cgllc.magnet.hodor/) | [说明书](https://home.mi.com/views/introduction.html?model=cgllc.magnet.hodor&region=cn)

![cgllc.magnet.hodor](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-user-center/developer_1679070103597EkaI1qRs.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=RTYUpklgEyD7STOyAIAvokM3X5g=)

## 规格  
> 
**设备 ID** ：cgllc.magnet.hodor  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
关闭、开启、超过1分钟未关
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
