---
sidebar_label: '青萍动作和环境光传感器'
---
# 青萍动作和环境光传感器

2021-03-20 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=cgllc.motion.cgpr1/) | [说明书](https://home.mi.com/views/introduction.html?model=cgllc.motion.cgpr1&region=cn)

![cgllc.motion.cgpr1](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-user-center/developer_1679047724978kfgGugkB.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=yTESkwDjeVOzQvdnsPbT58GyHqI=)

## 规格  
> 
**设备 ID** ：cgllc.motion.cgpr1  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
有人移动、无人移动 1 分钟、无人移动 2 分钟、无人移动 5 分钟、无人移动 10 分钟、无人移动 30 分钟、有人移动且光照强度高于、有人移动且光照强度低于、光照强度高于、光照强度低于、光照较强、光照较弱、无人移动
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
