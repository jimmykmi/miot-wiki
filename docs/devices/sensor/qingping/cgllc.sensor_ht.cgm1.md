---
sidebar_label: '李吉他蓝牙温湿度计'
---
# 李吉他蓝牙温湿度计

2020-09-17 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=cgllc.sensor_ht.cgm1/) | [说明书](https://home.mi.com/views/introduction.html?model=cgllc.sensor_ht.cgm1&region=cn)

![cgllc.sensor_ht.cgm1](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-user-center/developer_16790476877363hBHIjPf.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=aBv7B87l0cr2/Y0p5HlWWOhh+dw=)

## 规格  
> 
**设备 ID** ：cgllc.sensor_ht.cgm1  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
高于指定温度、低于指定温度、高于指定湿度、低于指定湿度
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
