---
sidebar_label: '多瑞燃气报警器'
---
# 多瑞燃气报警器

2023-03-31 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=rttrp.sensor_gas.jy500/) | [说明书](https://home.mi.com/views/introduction.html?model=rttrp.sensor_gas.jy500&region=cn)

![rttrp.sensor_gas.jy500](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-product-center/b43a80a18967c34f334d07692e383587_1637205451250.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=EQ9QvGmAgNZN+/d1chOchny6LWU=)

## 规格  
> 
**设备 ID** ：rttrp.sensor_gas.jy500  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event

:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
