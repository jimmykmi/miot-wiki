---
sidebar_label: '米家蓝牙温湿度计'
---
# 米家蓝牙温湿度计

2017-12-08 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=cleargrass.sensor_ht.dk1/) | [说明书](https://home.mi.com/views/introduction.html?model=cleargrass.sensor_ht.dk1&region=cn)

![cleargrass.sensor_ht.dk1](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-user-center/developer_1678870889179mZjUwpsA.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=NBRerXQo6k9Lv3nXhQy4d86G2Ts=)

## 规格  
> 
**设备 ID** ：cleargrass.sensor_ht.dk1  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
高于指定温度、低于指定温度、高于指定湿度、低于指定湿度
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
