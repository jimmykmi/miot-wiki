---
sidebar_label: '小米门窗传感器2'
---
# 小米门窗传感器2

2020-08-12 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=isa.magnet.dw2hl/) | [说明书](https://home.mi.com/views/introduction.html?model=isa.magnet.dw2hl&region=cn)

![isa.magnet.dw2hl](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-product-center/developer_15882313789856TfOG0Ab.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=B7ZRCrVr0DimiAwU6awFWXjg8Rg=)

## 规格  
> 
**设备 ID** ：isa.magnet.dw2hl  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
环境光暗、环境光亮、打开、关闭、超时未关
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
