---
sidebar_label: '小米水浸卫士'
---
# 小米水浸卫士

2020-10-26 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=lumi.flood.bmcn01/) | [说明书](https://home.mi.com/views/introduction.html?model=lumi.flood.bmcn01&region=cn)

![lumi.flood.bmcn01](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-user-center/developer_1679071134718z471xKuY.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=WG/5MDxkSLtlgGOrp5AmD8kFphk=)

## 规格  
> 
**设备 ID** ：lumi.flood.bmcn01  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
浸水报警、浸水解除
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
