---
sidebar_label: '小米人体传感器2'
---
# 小米人体传感器2

2020-12-22 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=lumi.motion.bmgl01/) | [说明书](https://home.mi.com/views/introduction.html?model=lumi.motion.bmgl01&region=cn)

![lumi.motion.bmgl01](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-user-center/developer_16790733074588yVwADq2.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=Ka8nyrFdAqJcvSKX1krjeaMEPyk=)

## 规格  
> 
**设备 ID** ：lumi.motion.bmgl01  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
有人移动、2分钟无人移动、5分钟无人移动、有人移动且环境光亮、有人移动且环境光暗、环境光亮、环境光暗、超时无人移动
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
