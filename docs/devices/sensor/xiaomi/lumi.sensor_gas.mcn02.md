---
sidebar_label: '小米天然气卫士'
---
# 小米天然气卫士

2021-05-17 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=lumi.sensor_gas.mcn02/) | [说明书](https://home.mi.com/views/introduction.html?model=lumi.sensor_gas.mcn02&region=cn)

![lumi.sensor_gas.mcn02](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-user-center/developer_1679071135034H07gQWGE.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=9zqLhHfP78m9dhwDR+JpMEuvaNw=)

## 规格  
> 
**设备 ID** ：lumi.sensor_gas.mcn02  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
天然气泄漏报警
:::

:::condition

:::

:::action
设备自检
:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
