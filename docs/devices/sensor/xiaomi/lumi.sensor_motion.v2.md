---
sidebar_label: '小米人体传感器'
---
# 小米人体传感器

2017-11-16 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=lumi.sensor_motion.v2/) | [说明书](https://home.mi.com/views/introduction.html?model=lumi.sensor_motion.v2&region=cn)

![lumi.sensor_motion.v2](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-user-center/developer_1679047509894EuYwDysp.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=8b1ZH0dlvoCQUGvLxww1iGWbqnA=)

## 规格  
> 
**设备 ID** ：lumi.sensor_motion.v2  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
有人移动、2分钟无人移动、5分钟无人移动、10分钟无人移动、20分钟无人移动、30分钟无人移动
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
