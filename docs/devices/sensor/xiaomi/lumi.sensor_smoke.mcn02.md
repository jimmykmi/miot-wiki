---
sidebar_label: '小米烟感卫士'
---
# 小米烟感卫士

2021-03-25 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=lumi.sensor_smoke.mcn02/) | [说明书](https://home.mi.com/views/introduction.html?model=lumi.sensor_smoke.mcn02&region=cn)

![lumi.sensor_smoke.mcn02](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-user-center/developer_1679071135130DElAB8WS.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=sTTgggg9wBkgf0LN0DbNVvEcs2c=)

## 规格  
> 
**设备 ID** ：lumi.sensor_smoke.mcn02  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
火灾报警
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
