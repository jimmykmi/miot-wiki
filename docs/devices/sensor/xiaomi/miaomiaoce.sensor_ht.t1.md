---
sidebar_label: '小米米家电子温湿度计Pro'
---
# 小米米家电子温湿度计Pro

2019-04-04 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=miaomiaoce.sensor_ht.t1/) | [说明书](https://home.mi.com/views/introduction.html?model=miaomiaoce.sensor_ht.t1&region=cn)

![miaomiaoce.sensor_ht.t1](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-user-center/developer_16790475782639pVWWybx.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=Tx/jzKi/E7JKGfrc4Z9D0at3rak=)

## 规格  
> 
**设备 ID** ：miaomiaoce.sensor_ht.t1  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
高于指定温度、低于指定温度、高于指定湿度、低于指定湿度
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
