---
sidebar_label: '米家智能温湿度计3'
---
# 米家智能温湿度计3

2022-12-20 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=miaomiaoce.sensor_ht.t9/) | [说明书](https://home.mi.com/views/introduction.html?model=miaomiaoce.sensor_ht.t9&region=cn)

![miaomiaoce.sensor_ht.t9](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-product-center/e74381666cbe00560a9597c9d55ec111_1664180153032.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=mIusn6XQyAkCLeVeLj++feX0l8c=)

## 规格  
> 
**设备 ID** ：miaomiaoce.sensor_ht.t9  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
温度高于、温度低于、湿度高于、湿度低于、温度升高到、温度降低到、湿度升高到、湿度降低到、电量降低到
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
