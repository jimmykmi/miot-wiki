---
sidebar_label: 'Yeelight Pro 门窗传感器'
---
# Yeelight Pro 门窗传感器

2021-09-07 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=yeelink.magnet.ycs02/) | [说明书](https://home.mi.com/views/introduction.html?model=yeelink.magnet.ycs02&region=cn)

![yeelink.magnet.ycs02](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-product-center/ecfcf786790668cfada0a67685ca6c2d_1625474514223.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=4Z5FyHnInQ7rIWA38ca6hyo2Bqg=)

## 规格  
> 
**设备 ID** ：yeelink.magnet.ycs02  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
闭合、打开
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
