---
sidebar_label: 'Yeelight Pro 人体传感器'
---
# Yeelight Pro 人体传感器

2021-09-07 发布 | [产品百科](https://home.mi.com/webapp/content/baike/product/index.html?model=yeelink.motion.ymt02/) | [说明书](https://home.mi.com/views/introduction.html?model=yeelink.motion.ymt02&region=cn)

![yeelink.motion.ymt02](https://cdn.cnbj1.fds.api.mi-img.com/iotweb-product-center/d325b4b4dec48d8f84e6e41497187e3e_1625204977319.png?GalaxyAccessKeyId=AKVGLQWBOVIRQ3XLEW&Expires=9223372036854775807&Signature=WnIYxfiEhyaA1+byqTMgkva3FPs=)

## 规格  
> 
**设备 ID** ：yeelink.motion.ymt02  
**通讯协议** ：  
**极客版**  ：✔️触发 | ✔️查询 | ❌执行  
**最大距离** ：  
**最大角度** ：  

## 米家APP自动化  

:::event
有人移动、1分钟无人移动
:::

:::condition

:::

:::action

:::

## 米家自动化极客版  

:::event

:::

:::condition

:::

:::condition 可查询的状态条件

:::

:::action

:::

        
